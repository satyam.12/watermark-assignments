    Assessments on Elm:

    1. Use the github api “https://api.github.com/users” and display the user information(id, login, and avatar_url(image)) in the tabular format.
    
    2. You have to create an elm application with one single page, having two input types for inserting Years(Validation should be implemented). You have to print all the Leap Year between these two entered years. Entered years should not be included in the result.
